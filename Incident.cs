﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_project.Models
{
    public class Incident
    {
        public int Id { get; set; }
        public string LocName { get; set; }
        public DateTime Date { get; set; }

        public bool Status { get; set; }
        public bool EmailSent { get; set; }

    }
}
